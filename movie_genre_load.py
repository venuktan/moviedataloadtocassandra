from cassandra.cluster import Cluster
import time
import sys

__author__ = 'venuktangirala'


cluster = Cluster(['localhost'])
session = cluster.connect()
session.set_keyspace('moviedata')

class MovieLoad:
    customer_id= ''
    customer_name= ''
    active= ''
    time_stamp= ''
    pause_time = ''
    rating = ''
    movie_id= ''
    movie_name= ''
    movie_rating_date= ''
    movie_run_time= ''

    def __init__(self):
        self




    def parse_data(self,file1):

        data = open(file1,'r')

        data = data.readlines()

        # print session.execute("select * from movies_genre LIMIT 3")
        # session.execute("INSERT INTO movies_genre (genre, release_year ,movie_id,duration,movie_name) VALUES ('horror', 2000, 4728, 78,'lll');")

        # stmt= "BEGIN  BATCH"+" INSERT INTO movies_genre (genre, release_year ,movie_id,duration,movie_name) VALUES (?,?,?,?,?)"+" INSERT INTO movies_genre (genre, release_year ,movie_id,duration,movie_name) VALUES (?,?,?,?,?)"+" APPLY BATCH;"
        # print stmt
        # prepared = session.prepare(stmt)
        # session.execute(prepared.bind(('documentary', 1998, 3253, 59,'The Opposite of Sex','action', 1984, 1618, 60,'Nausicaa of the Valley of the Wind')))

        loopAppendStmt="BEGIN  BATCH"
        insertStatement = " INSERT INTO movies_genre (genre, release_year ,movie_id,duration,movie_name) VALUES (?,?,?,?,?)"

        for i in range(0,2000):
            loopAppendStmt=loopAppendStmt+insertStatement

        loopAppendStmt=loopAppendStmt+" APPLY BATCH;"
        prepared=session.prepare(loopAppendStmt)
        values=[]

        i=0

        # print loopAppendStmt

        for line in data:
            i=i+1

            beforeMovieName=line.split(', "')
            beforeMovieName_splits = beforeMovieName[0].split(',')

            customer_id =beforeMovieName_splits[0]
            customer_name = beforeMovieName_splits[1]
            active = beforeMovieName_splits[2]
            time_stamp = beforeMovieName_splits[3]
            pause_time= beforeMovieName_splits[4]
            rating= beforeMovieName_splits[5]
            movie_id= beforeMovieName_splits[6]

            # print beforeMovieName[0]

            movie_afterMovie = beforeMovieName[1]
            movie_afterMovie = movie_afterMovie.split('",')

            movie_name = movie_afterMovie[0].replace("'","")
            afterMovieName = movie_afterMovie[1]

            afterMovieName_splits= afterMovieName.split(',')

            release_year = afterMovieName_splits[0]
            duration= afterMovieName_splits[1]
            genre= afterMovieName_splits[2].strip()

            #### load to cassandra####

            # perRecordInsertStatement=insertStatement+"'"+str(genre)+"',"+str(release_year)+","+str(movie_id)+","+str(duration)+",'"+str(movie_name)+"');"
            # perRecordInsertStatement.strip()
            # perRecordInsertStatement.rstrip("\n")
            # # print perRecordInsertStatement
            # # session.execute(perRecordInsertStatement)

            # values=values+"'"+str(genre)+"',"+str(release_year)+","+str(movie_id)+","+str(duration)+",'"+str(movie_name)+"'"

            values.append(str(genre))
            values.append(int(release_year))
            values.append(int(movie_id))
            values.append(int(duration))
            values.append(str(movie_name))


            if (i%2000==0 ):
                # print values
                print "inserted"+ str(i)
                session.execute_async(prepared.bind((values)))
                values=[]
        data.close()


        # print customer_id+customer_name+active+time_stamp+pause_time+rating+movie_id+movie_name+release_year+duration+genre


def readData():
    print str( session.execute("SELECT count(*) FROM moviedata.movies_genre LIMIT 100000;") )

if __name__ == '__main__':
    start=time.clock()
    # print sys.argv[1]
    # MovieLoad().parse_data(sys.argv[1])

    elaspsed= time.clock() - start
    print "time elapsed for writing:"+str(elaspsed)

    time.sleep(1)

    readData()





